Source: dh-dlang
Section: devel
Priority: optional
Maintainer: Debian D Language Group <team+d-team@tracker.debian.org>
Uploaders: Matthias Klumpp <mak@debian.org>
Build-Depends: debhelper (>= 11),
               flake8
Standards-Version: 4.2.0
Vcs-Git: https://salsa.debian.org/d-team/dh-dlang.git
Vcs-Browser: https://salsa.debian.org/d-team/dh-dlang

Package: dh-dlang
Architecture: all
Depends: debhelper (>= 11),
         default-d-compiler,
         python3,
         ${misc:Depends}
Description: Packaging helpers for building D code
 This package contains common code for building D software for
 the Debian (and derivatives) archive.
 .
 It currently contains a debian/rules snippet for selecting the
 appropriate D build flags based on the default D compiler set
 for the current architecture.

Package: default-d-compiler
Architecture: amd64 arm64 armel armhf i386 ppc64 ppc64el x32
Depends: gdc (>= 4:8) [armel ppc64el ppc64 x32],
         ldc (>= 1:1.11) [amd64 arm64 i386 armhf],
         ${misc:Depends}
Description: Default D compiler (metapackage)
 This is a metapackage installing the default D compiler in Debian
 for the respective architecture.
 .
 Packages building D libraries or using them should depend on this.
