#
# Set the DFLAGS global variable matching the current default D compiler on the given architecture,
# to apply the D flags we want for release builds in Debian.
# This also applies arch-specific quirks, like not building with NEON on armhf when LDC is used.
#
# This script is supposed to be included by debian/rules directly
#

ifeq (,$(findstring not-installed, $(shell dpkg-query -W -f='$${Status}' ldc)))
    DC_LDC_FOUND := yes
endif

ifeq (,$(findstring not-installed, $(shell dpkg-query -W -f='$${Status}' gdc)))
    DC_GDC_FOUND := yes
endif

# by default, use GCC/GDC Debian optimization flags for D
DFLAGS = $(CFLAGS)

# set the right D flags on architectures where LDC is default
ifneq (,$(findstring $(DEB_HOST_ARCH), amd64 i386 armhf))
    # only set LDC build flags if our compiler is LDC
    ifeq ($(DC_LDC_FOUND),yes)
        DFLAGS=-O -g -release -wi
        ifeq ($(DEB_HOST_ARCH),armhf)
            DFLAGS += -mattr=-neon
        endif
    endif
endif
export DFLAGS
