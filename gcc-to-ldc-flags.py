#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Matthias Klumpp <mak@debian.org>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#
# The same code is also part of the Meson build system and available under
# the terms of the Apache License, Version 2.0

import os
import sys


def translate_args_to_nongnu(args):
    dcargs = []
    # Translate common arguments to flags the LDC/DMD compilers
    # can understand.
    # The flags might have been added by pkg-config files,
    # and are therefore out of the user's control.
    for arg in args:
        if arg == '-pthread':
            continue
        if arg.startswith('-Wl,'):
            linkargs = arg[arg.index(',') + 1:].split(',')
            for la in linkargs:
                dcargs.append('-L' + la.strip())
            continue
        elif arg.startswith('-l'):
            # translate library link flag
            dcargs.append('-L' + arg)
            continue
        elif arg.startswith('-L/') or arg.startswith('-L./'):
            # we need to handle cases where -L is set by e.g. a pkg-config
            # setting to select a linker search path. We can however not
            # unconditionally prefix '-L' with '-L' because the user might
            # have set this flag too to do what it is intended to for this
            # compiler (pass flag through to the linker)
            # Hence, we guess here whether the flag was intended to pass
            # a linker search path.
            dcargs.append('-L' + arg)
            continue
        dcargs.append(arg)

    return dcargs


def main():
    if len(sys.argv) <= 1:
        print()
        return

    gcc_flags = sys.argv[1:]
    if not gcc_flags:
        gcc_flags = os.environ.get('LDFLAGS')
    if not gcc_flags:
        print()
        return

    print(' '.join(translate_args_to_nongnu(gcc_flags)))


if __name__ == '__main__':
    main()
